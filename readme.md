# My Verrrryyyyyyy Awesome Project Yeah

### Git Flow Guidelines

Install Git FLow first in you local machines
> (macos | homebrew) $ brew install git-flow-avh
>(macos | macports) $ port install git-flow-avh
>(linux) apt install git-flow
>(windows-cygwin)  wget -q -O - --no-check-certificate https://raw.github.com/petervanderdoes/gitflow-avh/develop/contrib/gitflow-installer.sh install stable | bash

#### Initialize Git Flow in Existing Project

> $ git clone [project-name]
> $ git flow init

Select **master** as the source branch

#### Starting a New Feature

> $ git flow feature start [job-order-example]

If two or more developers are assigned to a specific feature, publish (**push**) it in remote via

> $ git flow feature publish [job-order-example]

and pull it via

> $ git flow feature pull origin [job-order-example]

or if branch do not exist yet,

> $ git flow feature track [job-order-example]

If feature is done and tested (hopefully), merge to develop via

> $ git flow feature finish [job-order-example]

### Starting a Release

Our release tag format is:

> v1.0.0.0

Tag: **v**(version)**.**1(major release)**.0**(minor release)**.1**(hotfix bug fixes)**.1**(hotfix enhancement fixes)

Start the release

> $ git flow release start v1.0.0.0

If two or more developers are assigned to fix release, publish (**push**) it in remote via

> $ git flow release publish v1.0.0.0

and pull it via

> $ git pull

or pull if branch do not exist yet,

> $ git flow release track v1.0.0.0

If release is done and tested (hopefully), merge to develop via

> $ git flow release finish v1.0.0.0

### Hotfixes

Start a Hotfix via

> $ git flow hotfix start v1.0.1.0

If two or more developers are assigned to fix hotfixes, publish (**push**) it in remote via

> $ git flow hotfix publish v1.0.1.0

and pull it via

> $ git pull

or pull if branch do not exist yet,

> $ git flow hotfix track v1.0.1.0

If hotfix is done and tested (hopefully), merge to develop via

> $ git flow hotfix finish v1.0.1.0

#### Release and Hotfixes branches must be created only by the Repo Master

